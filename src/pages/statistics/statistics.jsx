import { hot } from "react-hot-loader/root";
import React from "react";
import Page, { Grid, GridColumn } from '@atlaskit/page';

import { Profile, Team } from './components'
import image from "./images/burndown.png"

import styles from './style.css'

const teamTasks = [
  {
    id: "10 с-п",
    name: "Дмитрий Маркелов",
    taskName: "Main task",
    presence: "online",
    url: "google.com",
  },
  {
    id: "9 с-п",
    name: "Николай Клюев",
    taskName: "Main task 2",
    presence: "offline",
    url: "google.com",
  },
  {
    id: "7 с-п",
    name: "Иван Киященко",
    taskName: "Main task 3",
    presence: "offline",
    url: "google.com",
  },
  {
    id: "6 с-п",
    name: "Дмитрий Остапенко",
    taskName: "Main task 4",
    presence: "offline",
    url: "google.com",
  },
]

const Statistics = (props) => {

  return (
    <Page>
      <Grid>
        <GridColumn medium={5}>
          <Profile />
        </GridColumn>
        <GridColumn medium={7}>
          <Team items={teamTasks} />
        </GridColumn>
      </Grid>
      <Grid>
        <img className={styles.chart} src={image} />
      </Grid>
    </Page>
  );
}

export default hot(Statistics)
