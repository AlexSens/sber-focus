import React from "react";
import _ from 'lodash'

import theme from "../../../../common/theme.css"

import { Item } from './item'
import styles from './style.css'

export const Team = (props) => (
  <div>
    <ul className={styles.container}>
      {_.map(props.items, (item, key) => (
        <li className={styles.item} key={key}>
          <Item {...item} />
        </li>
      ))}
    </ul>
  </div>
)
