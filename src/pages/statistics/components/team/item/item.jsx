import React from "react";
import Avatar from '@atlaskit/avatar';
import ReactTooltip from 'react-tooltip'
import Lozenge from '@atlaskit/lozenge';

import { Container } from '../../../../../common/components'

import styles from './style.css'

export const Item = (props) => (
  <Container>
    <div className={styles.container}>
      <div className={styles.lozenge}>
        <Lozenge appearance='new'>{props.id}</Lozenge>
      </div>
      <Avatar
        enableTooltip={true}
        size="large"
        presence={props.presence}
        href={props.url}
      />
      <p className={styles["task-name"]}>{props.name}</p>
    </div>
  </Container>
)
