import React from "react";
import Avatar from '@atlaskit/avatar';
import _ from 'lodash'

import { Container } from '../../../../common/components'

import styles from './style.css'

export const Profile = (props) => (
  <Container>
    <div className={styles.container}>
      <Avatar
        size="xxlarge"
        presence='online'
        src="https://sun9-11.userapi.com/c855632/v855632374/4f3fc/_nkD1h_Ny54.jpg"
      />
      <div className={styles.info}>
        <div className={styles.name}>Александр</div>
        <div className={styles.status}>Главный кочегар</div>
        <div className={styles.points}>12 стори поинтов</div>
      </div>
    </div>
  </Container>
)
