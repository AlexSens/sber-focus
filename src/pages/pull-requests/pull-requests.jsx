import { hot } from "react-hot-loader/root";
import React from "react";
import Page, { Grid, GridColumn } from '@atlaskit/page';

import { My, Other } from './components'

import styles from './style.css'

const myPR = [
  {
    id: 'wr-22',
    title: 'RLM-189 export-import services with workflow',
    name: 'Тимонин Александр Викторович',
    date: 'сегодня',
    status: 'inprogress',
  },
  {
    id: 'wr-23',
    title: 'RLM-187 Main page theming.',
    name: 'Киященко Иван Сергеевич',
    date: '2 дня назад',
    status: 'moved',
    src: 'https://sun9-20.userapi.com/c857632/v857632335/df6d/2Hp7tFcBG6c.jpg?ava=1'
  },
]

const otherPR = [
  {
    id: 'wr-221',
    title: 'RLM-124 create store',
    name: 'Маркелов Дмитрий Сергеевич',
    date: 'сегодня',
    status: 'inprogress',
    isButton: false,
    src: 'https://sun9-25.userapi.com/c9558/v9558225/4/HUlSFxzMuNA.jpg'
  },
  {
    id: 'wr-232',
    title: 'RLM-130 create ide plugin',
    name: 'Маркелов Дмитрий Сергеевич',
    date: '3 дня назад',
    status: 'moved',
    isButton: true,
    src: 'https://sun9-25.userapi.com/c9558/v9558225/4/HUlSFxzMuNA.jpg'
  },
  {
    id: 'wr-233',
    title: 'RLM-132 update main page footer',
    name: 'Маркелов Дмитрий Сергеевич',
    date: '4 дня назад',
    status: 'removed',
    isButton: true,
    src: 'https://sun9-25.userapi.com/c9558/v9558225/4/HUlSFxzMuNA.jpg'
  },
]

const PullRequests = (props) => {

  return (
    <Page>
      <Grid>
        <GridColumn medium={6}>
          <My items={myPR} />
        </GridColumn>
        <GridColumn medium={6}>
          <Other items={otherPR} />
        </GridColumn>
      </Grid>
    </Page>
  );
}

export default hot(PullRequests)
