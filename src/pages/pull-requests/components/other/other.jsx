import React from "react";
import _ from 'lodash'

import theme from "../../../../common/theme.css"

import { Item } from './item'
import styles from './style.css'

export const Other = (props) => (
  <div>
    <h3 className={theme["container-title"]}>Мои запросы в обработке</h3>
    <ul className={styles.container}>
      {_.map(props.items, (item, key) => (
        <li className={styles.item} key={key}>
          <Item {...item} />
        </li>
      ))}
    </ul>
  </div>
)
