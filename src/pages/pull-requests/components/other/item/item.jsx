import React from "react";
import Avatar from '@atlaskit/avatar';
import ReactTooltip from 'react-tooltip'
import Lozenge from '@atlaskit/lozenge';

import { Container } from '../../../../../common/components'

import styles from './style.css'

export const Item = (props) => (
  <Container>
    <div className={styles.container}>
      <div className={styles.lozenge}>
        <Lozenge appearance={props.status}>{props.date}</Lozenge>
      </div>
      <Avatar
        enableTooltip={true}
        size="large"
        presence='online'
        href={props.url}
        src={props.src}
      />
      <div className={styles.info}>
        <div className={styles.title}>{props.title}</div>
        <div className={styles.name}>{props.name}</div>
      </div>
      { props.isButton && <button className={styles.button}>Напомнить</button>}
    </div>
  </Container>
)
