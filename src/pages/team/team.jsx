import { hot } from "react-hot-loader/root";
import React from "react";
import Page, { Grid, GridColumn } from '@atlaskit/page';

import { Teammates, Protocols } from './components'

import styles from './style.css'

const Team = (props) => {

  return (
    <Page>
      <Grid>
        <GridColumn medium={6}>
          <Teammates />
        </GridColumn>
        <GridColumn medium={6}>
          <Protocols />
        </GridColumn>
      </Grid>
    </Page>
  );
}

export default hot(Team)
