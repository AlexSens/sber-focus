import React from "react";
import _ from 'lodash'

import theme from "../../../../common/theme.css";

import { Item } from './item'
import styles from './style.css'

const items = [
  {
    name: "Николай Клюев",
    role: "Разработчик",
    presence: "online",
    url: "https://www.google.com/",
    src: "https://sun9-5.userapi.com/c858124/v858124558/57750/GMxWpeLFpe0.jpg?ava=1"
  },
  {
    name: "Иван Киященко",
    role: "Разработчик",
    presence: "online",
    url: "https://www.google.com/",
    src: "https://sun9-20.userapi.com/c857632/v857632335/df6d/2Hp7tFcBG6c.jpg?ava=1"
  },
  {
    name: "Дмитрий Маркелов",
    role: "Разработчик",
    presence: "offline",
    url: "https://www.google.com/",
    src: "https://sun9-62.userapi.com/c631228/v631228225/29510/mmruvbC0SaA.jpg?ava=1"
  },
  {
    name: "Александр Тимонин",
    role: "Разработчик",
    presence: "online",
    url: "https://www.google.com/",
    src: "https://sun9-54.userapi.com/c855632/v855632374/4f404/-LWW0WDtkkc.jpg?ava=1"
  },
]

export const Teammates = (props) => (
  <div className={styles.container}>
    <h3 className={theme["container-title"]}>Мои коллеги по проекту «Разгром»</h3>
    <div className={styles.events}>
      {_.map(items, (item, key) => (
        <div className={styles.item}>
          <Item item={item} key={key} />
        </div>
      ))}
    </div>
  </div>
)
