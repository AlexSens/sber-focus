import React from "react";
import Avatar from '@atlaskit/avatar';

import { Container } from "../../../../../common/components/container";

import styles from './style.css'

export const Item = (props) => (
  <Container>
    <div className={styles.container}>
      <Avatar
        enableTooltip={true}
        size="large"
        presence={props.item.presence}
        href={props.item.url}
        src={props.item.src}
      />
      <div className={styles.info}>
        <div className={styles.name}>{props.item.name}</div>
        <div className={styles.role}>{props.item.role}</div>
      </div>
    </div>
  </Container>
)
