import React from "react";
import _ from 'lodash'
import Button from '@atlaskit/button'

import theme from "../../../../common/theme.css";

import { Item } from './item'
import styles from './style.css'

const items = [
  {
    name: "Встреча 221019",
  },
  {
    name: "Результат обсуждений 100919",
  },
]

export const Protocols = (props) => (
  <div className={styles.container}>
    <h3 className={theme["container-title"]}>Протоколы встреч</h3>
    <div className={styles.events}>
      {_.map(items, (item, key) => (
        <div className={styles.item}>
          <Item item={item} key={key} />
        </div>
      ))}
    </div>
    <button className={styles.button}>+ ДОБАВИТЬ НОВЫЙ ПРОТОКОЛ</button>
  </div>
)
