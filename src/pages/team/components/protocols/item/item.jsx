import React from "react";

import { Container } from "../../../../../common/components/container";

import icon from './images/confluence.png'
import styles from './style.css'

export const Item = (props) => (
  <Container>
    <div className={styles.container}>
      <img src={icon} alt="icon" className={styles.icon} />
      <div className={styles.name}>{props.item.name}</div>
    </div>
  </Container>
)
