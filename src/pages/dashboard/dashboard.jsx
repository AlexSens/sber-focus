import { hot } from "react-hot-loader/root";
import React, { useState, useEffect} from "react";
import { createStructuredSelector } from "reselect";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux'
import classNames from 'classnames'
import Page, { Grid, GridColumn } from '@atlaskit/page';
import Banner from '@atlaskit/banner';
import WarningIcon from '@atlaskit/icon/glyph/warning';

import { getDashboard } from '../../__data__/actions/dashboard'
import * as selectors from '../../__data__/selectors/dashboard'
import { PageSpinner } from '../../common/components/page-spinner';
import { ActiveTask, TeamTasks, OutstandingTasks, Events, CompletedTasks } from './components'

import styles from './style.css'

const Icon = <WarningIcon label="Warning icon" secondaryColor="inherit" />;

const teamTasks = [
  {
    id: "SBT-133",
    name: "Александр Тимонин",
    taskName: "Дашборд. Вёрстка.",
    presence: "online",
    url: "google.com",
  },
  {
    id: "SBT-323",
    name: "Дмитрий Маркелов",
    taskName: "Разводная. Запросы.",
    presence: "offline",
    url: "https://sun9-25.userapi.com/c9558/v9558225/4/HUlSFxzMuNA.jpg",
  },
  {
    id: "SBT-143",
    name: "Киященко Иван",
    taskName: "Chrome extensions",
    presence: "offline",
    url: "https://sun9-4.userapi.com/c857632/v857632335/df66/Qq9varRiEjY.jpg",
  },
  {
    id: "SBT-253",
    name: "Клюев Николай",
    taskName: "Плагин IDE",
    presence: "offline",
    url: "https://sun9-45.userapi.com/c846418/v846418101/177921/l5maWSqwjvE.jpg",
  },
]

const outstandingTasks = [
  {
    name: 'История операций',
    url: 'google.com',
  },
  {
    name: 'Разработать boilerplate',
    url: 'google.com',
  },
  {
    name: 'Актуализировать карты.',
    url: 'google.com',
  },
  {
    name: 'Анализ данных',
    url: 'google.com',
  },
  {
    name: 'UI тестирование.',
    url: 'google.com',
  },
  {
    name: 'Вёрстка разводной',
    url: 'google.com',
  },
  {
    name: 'Хлебные крошки',
    url: 'google.com',
  },
  {
    name: 'Расчёт стоимости пая',
    url: 'google.com',
  },
  {
    name: 'Актуализировать макеты',
    url: 'google.com',
  },
]

const completedTasks = [
  {
    name: 'Перенос данных',
    url: 'https://www.google.com/',
  },
  {
    name: 'Ускорить анимацию',
    url: 'https://www.google.com/',
  },
]

const events = [
  {
    name: 'Вы приступили к задаче с самым низким приоритетом'
  },
  {
    name: 'Ваш пулл реквест не рассматривают уже 4 дня'
  },
  {
    name: 'Вы 3 дня не совершали коммитов'
  },
  {
    name: 'Пора приступать к следующей задаче'
  },
]

const Dashboard = (props) => {
  const { getDashboard, isLoading, isError } = props

  const [isWarning, setWarning] = useState(false);

  useEffect(() => {
    getDashboard()
  }, [])

  return (
    <Page>
      { isLoading ?
        <PageSpinner /> :
        <div>
          <div className={classNames(isWarning && styles["open-warning-wrapper"])}>
            <Banner isOpen={isWarning} appearance="warning" icon={Icon}>
              Задача UI-214 имеет высокий приоритет. Начни с неё.
            </Banner>
          </div>
          <div>
            <Grid>
              <GridColumn medium={4}>
                <ActiveTask />
                <div onClick={() => setWarning(!isWarning)} className={styles.trigger} />
              </GridColumn>
              <GridColumn medium={8}>
                <TeamTasks items={teamTasks} />
              </GridColumn>
            </Grid>
          </div>
          <div className={styles.grid}>
            <Grid>
              <GridColumn medium={4}>
                <OutstandingTasks items={outstandingTasks} />
                <CompletedTasks items={completedTasks} />
              </GridColumn>
              <GridColumn medium={8}>
                <Events items={events} />
              </GridColumn>
            </Grid>
          </div>
        </div>
      }
    </Page>
  );
}

const mapStateToProps = () => createStructuredSelector({
  articles: selectors.getDashboard,
  isLoading: selectors.getDashboardIsLoading,
  isError: selectors.getDashboardIsError,
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  getDashboard
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(hot(Dashboard))
