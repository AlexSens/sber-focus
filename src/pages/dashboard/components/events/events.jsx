import React from "react";
import _ from 'lodash'

import theme from "../../../../common/theme.css";

import { Event } from './event'
import styles from './style.css'
import { Task } from "../outstanding-tasks/task";

export const Events = (props) => (
  <div className={styles.container}>
    <h3 className={theme["container-title"]}>События</h3>
    <div className={styles.events}>
      {_.map(props.items, (item, key) => (
        <div className={styles.item}>
          <Event item={item} key={key} />
        </div>
      ))}
    </div>
  </div>
)
