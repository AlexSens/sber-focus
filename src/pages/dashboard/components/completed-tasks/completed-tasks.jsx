import React from "react";
import _ from 'lodash'

import theme from "../../../../common/theme.css";
import { declOfNum } from "../../../../common/utils"

import { Item } from './item'
import styles from './style.css'

export const CompletedTasks = (props) => {
  const tasksCount = props.items.length
  const taskName = declOfNum(tasksCount, ['задача', 'задачи', 'задач'])

  return (
    <div className={styles.container}>
      <h3 className={theme["container-title"]}>{`Уже сделал ${tasksCount} ${taskName}`}</h3>
      <div className={styles.events}>
        {_.map(props.items, (item, key) => (
          <div className={styles.item}>
            <Item item={item} key={key} />
          </div>
        ))}
      </div>
    </div>
  )
}
