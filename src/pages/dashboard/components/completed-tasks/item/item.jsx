import React from "react";

import { Container } from "../../../../../common/components/container";

import styles from './style.css'

export const Item = (props) => (
  <Container>
    <div className={styles.container}>
      <div className={styles.name}>{props.item.name}</div>
    </div>
  </Container>
)
