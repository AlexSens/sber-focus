import React from "react";
import { ProgressTracker } from '@atlaskit/progress-tracker';
import Lozenge from '@atlaskit/lozenge';

import { Container } from '../../../../common/components'

import styles from './style.css'

const items = [
  {
    id: 'visited-1',
    percentageComplete: 100,
    status: 'disabled',
  },
  {
    id: 'current-1',
    percentageComplete: 100,
    status: 'disabled',
  },
  {
    id: 'unvisited-1',
    percentageComplete: 100,
    status: 'disabled',
  },
  {
    id: 'unvisited-2',
    percentageComplete: 0,
    status: 'current',
  },
  {
    id: 'unvisited-2',
    percentageComplete: 0,
    status: 'unvisited',
  }
];

export const ActiveTask = (props) => (
  <Container title="Я сейчас делаю">
    <div className={styles.container}>
      <div className={styles.lozenge}>
        <Lozenge appearance='default'>SBT-1442</Lozenge>
      </div>
      <p className={styles.title}>UI тестирование.</p>
      <ProgressTracker items={items} animated={true} />
    </div>
  </Container>
)
