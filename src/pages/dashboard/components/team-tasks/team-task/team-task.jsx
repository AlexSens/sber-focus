import React from "react";
import Avatar from '@atlaskit/avatar';
import ReactTooltip from 'react-tooltip'
import Lozenge from '@atlaskit/lozenge';

import { Container } from '../../../../../common/components'

import styles from './style.css'

export const TeamTask = (props) => (
  <Container>
    <div className={styles.container}>
      <div className={styles.lozenge}>
        <Lozenge appearance='new'>{props.id}</Lozenge>
      </div>
      <div data-tip data-for={props.id}>
        <Avatar
          enableTooltip={true}
          size="large"
          presence={props.presence}
          src={props.url}
        />
      </div>
      <ReactTooltip id={props.id} type='light'>
        <span>{props.name}</span>
      </ReactTooltip>
      <p className={styles["task-name"]}>{props.taskName}</p>
    </div>
  </Container>
)
