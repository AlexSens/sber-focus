import React from "react";
import _ from 'lodash'

import theme from "../../../../common/theme.css"

import { TeamTask } from './team-task'
import styles from './style.css'

export const TeamTasks = (props) => (
  <div>
    <h3 className={theme["container-title"]}>А мои коллеги</h3>
    <ul className={styles.container}>
      {_.map(props.items, (item, key) => (
        <li className={styles.item} key={key}>
          <TeamTask {...item} />
        </li>
      ))}
    </ul>
  </div>
)
