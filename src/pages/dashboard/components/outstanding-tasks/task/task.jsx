import React from "react";

import { Container } from '../../../../../common/components'

import styles from './style.css'

export const Task = (props) => (
  <Container>
    <a href={props.url} target="_blank" className={styles.link}>
      <div className={styles.container}>{props.item.name}</div>
    </a>
  </Container>
)
