import React from "react";
import _ from "lodash";

import theme from "../../../../common/theme.css";
import { declOfNum } from "../../../../common/utils"

import { Task } from './task'
import styles from './style.css'

export const OutstandingTasks = (props) => {
  const tasksCount = props.items.length
  const taskName = declOfNum(tasksCount, ['задача', 'задачи', 'задач'])

  return (
    <div className={styles.container}>
      <h3 className={theme["container-title"]}>{`До пятницы еще ${tasksCount} ${taskName}`}</h3>
      <div className={styles.items}>
        {_.map(props.items, (item, key) => (
          <div className={styles.item}>
            <Task item={item} key={key} />
          </div>
        ))}
      </div>
    </div>
  )
}
