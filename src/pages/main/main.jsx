import { hot } from "react-hot-loader/root";
import React from "react";
import Page, { Grid } from '@atlaskit/page';

import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import 'react-web-tabs/dist/react-web-tabs.css';

import { Dashboard, Statistics, PullRequests, Team } from '../'

import logo from './images/logo.png'

import styles from './style.css'

const Main = (props) => {

  return (
    <Page>
      <Grid>
        <div className={styles.page}>
          <img src={logo} className={styles.logo} />
          <div className={styles.tabs}>
            <Tabs
              defaultTab="one"
              onChange={(tabId) => { console.log(tabId) }}
            >
              <TabList>
                <Tab tabFor="one">Главное</Tab>
                <Tab tabFor="two">Pull requests</Tab>
                <Tab tabFor="three">Команда</Tab>
                <Tab tabFor="four">Статистика</Tab>
              </TabList>
              <TabPanel tabId="one">
                <Dashboard />
              </TabPanel>
              <TabPanel tabId="two">
                <PullRequests />
              </TabPanel>
              <TabPanel tabId="three">
                <Team />
              </TabPanel>
              <TabPanel tabId="four">
                <Statistics />
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </Grid>
    </Page>
  );
}

export default hot(Main)
