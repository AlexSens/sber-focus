import React from "react";
import Spinner from '@atlaskit/spinner';

import styles from './style.css'

export const PageSpinner = () => (
  <div className={styles.wrapper}>
    <Spinner
      size="xlarge"
      delay={3000}
    />
  </div>
)
