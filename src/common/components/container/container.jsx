import React from "react"

import theme from '../../theme.css'

import styles from './style.css'

export const Container = (props) => (
  <div>
    { props.title && <h3 className={theme["container-title"]}>{props.title}</h3> }
    <div className={styles.container}>
      {props.children}
    </div>
  </div>
)
