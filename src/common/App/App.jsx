import { hot } from "react-hot-loader/root";
import React from "react";
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import { storeCreator } from '../../__data__/store'
import { Dashboard, Main, Team, PullRequests, Statistics } from '../../pages'

const App = () => {
  const store = storeCreator()

  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/statistics">
            <Statistics />
          </Route>
          <Route path="/pull-requests">
            <PullRequests />
          </Route>
          <Route path="/team">
            <Team />
          </Route>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
          <Route path="/">
            <Main />
          </Route>
        </Switch>
      </Router>
    </Provider>
  )
}

export default hot(App);
