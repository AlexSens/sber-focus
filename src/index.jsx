import React from "react";
import ReactDOM from "react-dom";
import '@atlaskit/css-reset';

import App from "./common/App/App";

ReactDOM.render(<App />, document.getElementById("root"));
