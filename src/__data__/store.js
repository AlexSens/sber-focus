import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { devToolsEnhancer } from 'redux-devtools-extension/developmentOnly'
import thunkMiddleware from 'redux-thunk'

import combinedReducer from './reducers'

const packageJson = require('../../package.json')

export const STORE_NAMESPACE = packageJson.name

const reducer = combineReducers({
  [STORE_NAMESPACE]: combinedReducer,
})

const enhancer = () => compose(
  applyMiddleware(thunkMiddleware),
  devToolsEnhancer({ name: STORE_NAMESPACE })
)

export function storeCreator () {
  return createStore(reducer, enhancer())
}
