import { combineReducers } from 'redux'

import dashboard from './dashboard'

const reducers = {
  dashboard,
}

export default combineReducers({ ...reducers })
