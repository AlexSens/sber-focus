import { DASHBOARD_FETCH, DASHBOARD_FETCH_SUCCESS, DASHBOARD_FETCH_FAILURE } from '../constants';

export const initialState = {
  articles: [],
  isLoading: false,
  isError: false,
};

const ACTION_HANDLERS = {
  [DASHBOARD_FETCH]: (state) => ({ ...state, isLoading: true }),
  [DASHBOARD_FETCH_SUCCESS]: (state, { articles }) => ({
    ...state,
    articles,
    isLoading: false,
    isError: false
  }),
  [DASHBOARD_FETCH_FAILURE]: (state) => ({
    ...state,
    isLoading: false,
    isError: true
  })
}

export default (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
