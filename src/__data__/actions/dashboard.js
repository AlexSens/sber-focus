import axios from 'axios'
import _ from 'lodash'

import * as types from '../constants';

export const dashboardFetch = () => ({
  type: types.DASHBOARD_FETCH,
})

export const dashboardFetchSuccess = (id) => ({
  type: types.DASHBOARD_FETCH_SUCCESS,
  id,
})

export const dashboardFetchFailure = () => ({
  type: types.DASHBOARD_FETCH_FAILURE,
})

const getDashboard = (id = '') => (dispatch) => {
  dispatch(dashboardFetch())
  return axios.post(`/test/${id}`)
    .then((response) => {
      const articles = _.get(response, ['data', 'articles'], null)
      dispatch(dashboardFetchSuccess(articles))
    })
    .catch(() => {
      dispatch(dashboardFetchFailure())
    })
}

export { getDashboard }
