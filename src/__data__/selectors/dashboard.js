import { createSelector } from 'reselect'
import _ from 'lodash'

import { getIndexStore } from './utils'

export const getDashboardState = createSelector(getIndexStore, (state) => _.get(state, 'dashboard', {}))

export const getDashboard = createSelector(getDashboardState, (state) => _.get(state, 'articles', []))

export const getDashboardIsLoading = createSelector(getDashboardState, (state) => _.get(state, 'isLoading', false))

export const getDashboardIsError = createSelector(getDashboardState, (state) => _.get(state, 'isError', null))
