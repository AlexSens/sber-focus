import { createSelector } from 'reselect'

import { STORE_NAMESPACE } from '../store'

export const getIndexStore = createSelector((state) => state[STORE_NAMESPACE], (state) => state)
